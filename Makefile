# Master Makefile for the chromium-prefs stuff. Builds everything there is to build. Extensions in the extensions/
# directory have their own Makefiles, which will be called by this one.
#
# Available targets are:
#
#     all          -  Build everything (extensions, flags, policies, preferences).
#
#     extensions   -  Build the extensions.
#     flags        -  Build the chromium-flags.conf.
#     policies     -  Build the policies.
#     preferences  -  Build the master_preferences.
#
#     clean        -  Remove the built stuff.
#     rmkey        -  Remove the extensions' private keys.
#
#     install      -  Install everything that has been built in the proper location.
#
# There is no "uninstall" target, build a proper package for your distribution and install/uninstall using your
# package manager. There is also no "check" or "test" target, but all JSON files will be processed by "jq", which
# will exit with an error if a file is not valid. Oh yeah, "jq" is required… also Python and OpenSSL for the
# extensions.
#
# Possible extension directories:
#
#     /usr/share/chromium/extensions/
#     /opt/google/chrome/extensions/
#     /usr/share/google-chrome/extensions/
#
# Possible configuration directories:
#
#     /etc/chromium/
#     /etc/opt/chrome/
#
# Possible master_preferences locations:
#
#     /usr/lib/chromium/master_preferences
#     /opt/google/chrome/master_preferences
#
# Config directories seem fixed to these two values, I guess Chromium may also be installed in /usr/local/, Chrome
# seems to live in /opt but looks for extensions in /usr/share/google-chrome/extensions/ also… So we just use one
# variable per path, not trying to guess anything. Defaults are suitable for a proper Chromium installation. The
# variables are:
#
#     EXTBASE  -  Extension base dir,      default: /usr/share/chromium
#     PREFDIR  -  master_preferences dir,  default: /usr/lib/chromium
#     CONFDIR  -  Global config dir,       default: /etc/chromium
#
# Since some paths have to be written to some of the files during the build process, these variables must be set
# for all make targets (technically, some paths are required for some targets, but it's just easier to set them all
# for all targets, and the default target is "all" anyway…)!
#
# Output files will be placed in OUTDIR (defaults to ".out"). Extensions will also respect the OUTDIR variable in
# their own Makefiles (it will be set to an absolute path first).
#
# The "install" target will copy the files from OUTDIR to the correct location in DESTDIR. DESTDIR defaults to an
# empty string, i.e. default installation paths are relative to the system's root directory (/).
#
# IMPORTANT: DESTDIR should be set to an absolute path! Since extensions will use the same DESTDIR value, relative
# paths will be treated as relative to the extension directories by the extensions' Makefiles.

EXTBASE ?= /usr/share/chromium
PREFDIR ?= /usr/lib/chromium
CONFDIR ?= /etc/chromium
OUTDIR  ?= .out

ifeq ($(findstring chrome,$(EXTBASE)$(PREFDIR)$(CONFDIR)),chrome)
	TARGET = chrome
else
	TARGET = chromium
endif

# All policies, these will be placed in policies/available:
#
POLICIES := $(subst policies,$(OUTDIR)/policies/available,$(wildcard policies/*.mgd policies/*.rcd))
POLICIES := $(patsubst %.rcd,%.json,$(patsubst %.mgd,%.json,$(POLICIES)))

# Managed policies, will be linked in policies/managed/:
#
MGDLINKS := $(subst policies,$(OUTDIR)/policies/managed,$(wildcard policies/*.mgd))
MGDLINKS := $(MGDLINKS:.mgd=.json)

# Recommended policies, will be linked in policies/recommended/:
#
RCDLINKS := $(subst policies,$(OUTDIR)/policies/recommended,$(wildcard policies/*.rcd))
RCDLINKS := $(RCDLINKS:.rcd=.json)

# Need these later to build the extension ID list…
#
MPT :=
SPC := $(MPT) $(MPT)
SEP := ", "

# Targets: --------------------------------------------------------------------------------------------------------

all: extensions flags policies preferences

#------------------------------------------------------------------------------------------------------------------

extensions:
	$(MAKE) -C extensions

#------------------------------------------------------------------------------------------------------------------

flags: $(OUTDIR)/commandline/chromium-flags.conf

$(OUTDIR)/commandline/chromium-flags.conf: commandline/chromium-flags.conf | $(OUTDIR)/commandline
	sed -E -e '/^$$/d' -e '/^#:/d' -e ':a;$$!N;s/\n\s+//;ta;P;D' '$<' >'$@'

#------------------------------------------------------------------------------------------------------------------

preferences: $(OUTDIR)/preferences/master_preferences

$(OUTDIR)/preferences/master_preferences: preferences/master_preferences | $(OUTDIR)/preferences
	jq --indent 3 -S . <'$<' >'$@'

#------------------------------------------------------------------------------------------------------------------

policies: $(POLICIES) $(MGDLINKS) $(RCDLINKS)

$(OUTDIR)/policies/available/%.json: policies/%.* | extensionids $(OUTDIR)/policies/available
	env FORCEIDS='$(FRCIDS)' EXTIDS='$(EXTIDS)' BASEDIR='$(EXTBASE)' \
		envsubst '$$FORCEIDS $$EXTIDS $$BASEDIR' <'$<' | jq --indent 3 -S . >'$@'

$(OUTDIR)/policies/managed/%.json: $(OUTDIR)/policies/available/%.json | $(OUTDIR)/policies/managed
	ln -f -s -t '$(@D)' '../available/$(@F)'

$(OUTDIR)/policies/recommended/%.json: $(OUTDIR)/policies/available/%.json | $(OUTDIR)/policies/recommended
	ln -f -s -t '$(@D)' '../available/$(@F)'

#------------------------------------------------------------------------------------------------------------------

extensionids: extensions/registry
	$(eval FRCIDS := $(shell cat extensions/registry))
	$(eval EXTIDS := $(filter-out file://%,$(subst ;, ,$(FRCIDS))))
	$(eval FRCIDS := "$(subst $(SPC),$(SEP),$(FRCIDS))")
	$(eval EXTIDS := "$(subst $(SPC),$(SEP),$(EXTIDS))")

extensions/registry:
	$(MAKE) -C extensions .REGISTER

#------------------------------------------------------------------------------------------------------------------

$(OUTDIR)/commandline $(OUTDIR)/preferences:
	mkdir -p '$@'

$(OUTDIR)/policies/available $(OUTDIR)/policies/managed $(OUTDIR)/policies/recommended:
	mkdir -p '$@'

#------------------------------------------------------------------------------------------------------------------

clean:
	rm -rf $(OUTDIR)
	$(MAKE) -C extensions clean .CLEANREG

rmkey:
	$(MAKE) -C extensions rmkey

#------------------------------------------------------------------------------------------------------------------

install: install-extensions install-flags install-policies install-preferences

#------------------------------------------------------------------------------------------------------------------

install-extensions: extensions
	$(MAKE) -C extensions install

#------------------------------------------------------------------------------------------------------------------

install-flags: flags
	install -m 0644 -D $(OUTDIR)/commandline/chromium-flags.conf $(DESTDIR)/$(CONFDIR)/$(TARGET)-flags.conf

#------------------------------------------------------------------------------------------------------------------

install-policies: policies | $(DESTDIR)/$(CONFDIR)
	cp -av $(OUTDIR)/policies $(DESTDIR)/$(CONFDIR)

#------------------------------------------------------------------------------------------------------------------

install-preferences: preferences | $(DESTDIR)/$(PREFDIR)
	install -m 0644 -D $(OUTDIR)/preferences/master_preferences $(DESTDIR)/$(CONFDIR)/master_preferences
	ln -f -s -t $(DESTDIR)/$(PREFDIR)/ $(CONFDIR)/master_preferences

#------------------------------------------------------------------------------------------------------------------

$(DESTDIR) $(DESTDIR)/$(CONFDIR) $(DESTDIR)/$(PREFDIR):
	mkdir -p '$@'

#------------------------------------------------------------------------------------------------------------------

.PHONY: all clean extensions extids flags install policies preferences rmkey
.SUFFIXES:

# :indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=115: ##############################################
