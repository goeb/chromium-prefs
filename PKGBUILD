# Maintainer: Stefan Göbel < chromiumprefs ʇɐ subtype ˙ de >

pkgbase='chromium-prefs'
pkgname=( 'chromium-prefs' 'google-chrome-prefs' )
pkgver=19700101000000
pkgrel=1
arch=( 'any' )
url='https://gitlab.com/goeb/chromium-prefs'
license=( 'GPL3' )
makedepends=( 'gettext' 'jq' 'openssl' 'python' )

_chrome_build_opts=(
   'EXTBASE=/opt/google/chrome'
   'PREFDIR=/opt/google/chrome'
   'CONFDIR=/etc/opt/chrome'
)

_pkgbuild_dir=${_pkgbuild_dir:-$PWD}
_source_files=(
   'commandline'
   'extensions'
   'policies'
   'preferences'
   'ckupd'
   'crxid'
   'crxmk'
   'LICENSE'
   'Makefile'
   'PKGBUILD'
   'README'
)

pkgver() {

   date +%Y%m%d%H%M%S

}

prepare() {

   local _dest=''
   local _file=''

   for _dest in "${pkgname[@]}" ; do

      mkdir -p "$srcdir/$_dest"

      for _file in "${_source_files[@]}" ; do
         cp -avx "$_pkgbuild_dir/$_file" "$srcdir/$_dest/"
      done

      cd "$srcdir/$_dest"
      make clean

   done

}

build() {

   cd "$srcdir/chromium-prefs"
   make

   cd "$srcdir/google-chrome-prefs"
   make "${_chrome_build_opts[@]}"

}

package_chromium-prefs() {

   pkgdesc='My preferences for the Chromium browser.'
   optdepends=( 'chromium' )
   backup=(
      'etc/chromium/chromium-flags.conf'
      'etc/chromium/master_preferences'
   )

   cd "$srcdir/chromium-prefs"

   local _file=''
   for _file in .out/policies/available/*.json ; do
      backup+=( "${_file//.out/etc/chromium}" )
   done

   make install DESTDIR="$pkgdir"

}

package_google-chrome-prefs() {

   pkgdesc='My preferences for the Chrome browser.'
   optdepends=( 'google-chrome' )
   backup=(
      'etc/opt/chrome/chrome-flags.conf'
      'etc/opt/chrome/master_preferences'
   )

   cd "$srcdir/google-chrome-prefs"

   local _file
   for _file in .out/policies/available/*.json ; do
      backup+=( "${_file//.out/etc/opt/chrome}" )
   done

   make install DESTDIR="$pkgdir" "${_chrome_build_opts[@]}"

}

# :indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=78: ########