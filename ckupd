#!/usr/bin/bash
#
# Checks some web pages / source files for updates. Last results will be cached in the .cache directory. Requires
# the "update" command line parameter to replace the old results files with the current ones (also has to be used
# on first run), without the parameter it will just download and compare the files and then remove the new ones.
# Requires lynx to convert the HTML pages to text. Note: .cache will be placed in the current working directory.
#
# Switches:       <https://peter.sh/experiments/chromium-command-line-switches/>
# Policies (old): <https://www.chromium.org/administrators/policy-list-3>
# Policies (new): <https://chromeenterprise.google/policies/>
# Settings:       <https://raw.githubusercontent.com/chromium/chromium/master/chrome/common/pref_names.cc>

declare -r cachedir='.cache'
declare -a lynx_opt=( '-dump' '-width=1024' '-nolist' '-stderr' '-display_charset=utf8' )
declare -a diff_opt=( '--color=always' '--unified=0' '--expand-tabs' '--tabsize=3' '--ignore-all-space' )
declare -a diff_ign=( '-I' 'powered by WordPress' '-I' 'Last automated update occurred on' )
declare -A src_urls=(
   ['switches']='https://peter.sh/experiments/chromium-command-line-switches/'
   ['policies']='https://www.chromium.org/administrators/policy-list-3'
   ['settings']='https://raw.githubusercontent.com/chromium/chromium/master/chrome/common/pref_names.cc'
   ['pol_json']='https://chromeenterprise.google/static/json/policy_templates_en-US.json'
)

mkdir -p "$cachedir"

for what in "${!src_urls[@]}" ; do

   base="$cachedir/$what"

   lynx "${lynx_opt[@]}" "${src_urls[$what]}" >"$base.new"

   if [[ -e "$base.old" ]] ; then
      diff "${diff_opt[@]}" "${diff_ign[@]}" "$base.old" "$base.new"
   else
      printf 'Old %s file not found.\n' "$what" >&2
   fi

   if [[ "${1:-}" == 'update' ]] ; then
      mv -f "$base.new" "$base.old"
   else
      rm -f "$base.new"
   fi

done

# :indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=115: --------------------------------------------